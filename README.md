# Description
Issue tracker with MEAN stack.

## Disclaimers
  - This project is opinionated in the sense that I opted for using **Yarn** and not **npm**. ~~Sorry&hellip;~~ not sorry!
  - You may want to adjust **angular.json** and edit or remove the **cli::packageManager** attribute(s).
  - This MEAN stack app has sibling front and backend folders and is by no means production&#45;ready (e.g. Heroku) which would infer a true need for a slight rearrangement (likely where the backend folder is the MAIN folder and the client folder would be contained within[&hellip; hint, hint](https://issuetracker-mean-demo.herokuapp.com "Link to The Issue App")). 
  - ~~The better elements of both the **Auth** and the **Todo/Issue tracker** projects will be fused into a single project for publication in the very near future and will likely be hosted at each **Mongo Atlas** and **Heroku**.~~
  - ~~It is unclear whether the source code for the proposed **fused** project will be open to the public at this time (it probably will be in the end).~~
  - This project is shared &quot;as is&quot; and the author (yours truly) is no longer responsible for any potential security or performance &quot;red flags&quot;; in the end, best practices were always a paramount consideration, however, as a one man operation even some of the most innocuous of details can slip through from time to time.

## Future Attributions
A lot of energy went into these project. I drew from my own experience with building a pure Express app hosted at Heroku but I, too, drew from many outside sources in order to arrive at the most presentable and user friendly presentation for immediate public consumption.

## No String Trimming helper functions for clean input
I ran out of time. But the MERN stack version of this project, indeed, has &lsquo;em.

## All glory and praise be to the Lord Jesus Christ!
I am nobody. He provides for my family despite the difficulties and purifying fires (take your pick). He deserves the praise and honor; I just have the faculties to comprehend this stuff. Yeah, yeah!

## If for some reason you missed it
In case you haven't caught on, this project can be viewed. Therefore, [please visit this repo](https://gitlab.com/artavia/issuetracker-mean-demo "Link to The Issue App") for more information. 