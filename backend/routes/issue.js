// BASE SETUP
// =============================================
const express = require("express");
const issueRouter = express.Router();

// MODEL DEFINITION
// =============================================
const Issue = require("../custom_models_mongoose/issue.model");

// ROUTING ASSIGNMENTS
// =============================================
issueRouter.route("/").get( ( req, res ) => {
  Issue.find( (err, issues) => {
    if(err){
      console.log( "err" , err );
    }
    res.json( issues );
  } );
} );

issueRouter.route("/:id").get( ( req, res ) => {
  
  let id = req.params.id;

  Issue.findById( id, ( err, issue ) => {
    if(err){
      console.log( "err" , err );
    }
    res.json(issue);
  } );
} );

issueRouter.route("/add").post( ( req, res ) => {

  /* 
  {
   "title" : "Freezerbox issues"
   , "responsible" : "Lucho"
   , "description" : "Ran out of chocolate ice cream"
   , "severity" : "High"
   , "status" : "Open"
  } 
  */
  

  // console.log( "req.body" , req.body ); 
  
  let issue = new Issue( req.body );
  issue.save().then( issue => {
    return res.status(200).json( { 'issue': 'issue added successfully' } );
  } ).catch( err => {
    return res.status(400).send( 'Add new issue failed, doofus!' );
  } );

} );



issueRouter.route("/update/:id").post( ( req, res, next ) => {
  
  let id = req.params.id;

  Issue.findById( id, ( err, issue ) => {
    if(!issue){
      // return res.status(404).send("Data not found... DOOFUS!")
      return next( new Error( "Data not found... DOOFUS!" ) );
    }
    issue.title = req.body.title;
    issue.responsible = req.body.responsible;
    issue.description = req.body.description;
    issue.severity = req.body.severity;
    issue.status = req.body.status;

    issue.save().then( issue => { return res.status(200).json('Issue updated') } ).catch( err => {
      return res.status(400).send( 'Update has failed, doofus!' );
    } );
  } );
} );

issueRouter.route("/delete/:id").get( ( req, res ) => {

  let idObjParameter = { _id: req.params.id };

  Issue.findByIdAndRemove( idObjParameter, ( err, issue ) => {
    if(err){
      console.log( "err" , err );
    }
    res.json( "Issue removed successfully" );
  } );

} );

module.exports = issueRouter;