// =============================================
// process.env SETUP
// =============================================
const { PORT , NODE_ENV, CONNECTION_URL } = process.env;

// new boilerplate
// =============================================
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

// BASE SETUP
// =============================================
const express = require("express");
const app = express();
const issueRouter = require("./routes/issue");

app.use( cors() );
app.use( bodyParser.json() );

// DB CONNECTION
// =============================================
mongoose.connect( CONNECTION_URL , { useNewUrlParser: true , useUnifiedTopology: true, useFindAndModify: false } );

const connection = mongoose.connection;
connection.once( "open" , () => { console.log( "MongoDB database connection established successfully" ); } );

app.use( '/issues' , issueRouter ); 
app.listen( PORT , () => { 
  // console.log( "process.env.CONNECTION_URL" , process.env.CONNECTION_URL );
  console.log( `Server is running on port ${PORT}` );   
} );