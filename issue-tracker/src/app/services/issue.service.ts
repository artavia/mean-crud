import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  uri = 'http://localhost:4000/issues';

  constructor( private http: HttpClient ) {}

  getIssues(){
    return this.http.get( `${this.uri}` );
  }

  getIssueById(id){
    return this.http.get( `${this.uri}/${id}` );
  }

  addIssue(title, responsible, description, severity ){
    const issue = {
      title: title, responsible: responsible, description: description, severity: severity 
    };
    return this.http.post( `${this.uri}/add` , issue );
  }

  updateIssue( id, title, responsible, description, severity, status ){
    const issue = {
      title: title, responsible: responsible, description: description, severity: severity, status: status 
    };
    return this.http.post( `${this.uri}/update/${id}` , issue );
  }

  deleteIssue( id ){
    return this.http.get( `${this.uri}/delete/${id}` );
  }

}
