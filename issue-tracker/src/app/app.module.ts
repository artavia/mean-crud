import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule, MatIconModule } from '@angular/material';
import { MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatButtonModule, MatCardModule, MatTableModule, MatDividerModule, MatSnackBarModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { ToolbarMultirowComponent } from './components/toolbar-multirow/toolbar-multirow.component';
// import { OldComponent } from './original/old/old.component';

@NgModule({
  declarations: [
    AppComponent
    , ListComponent
    , CreateComponent
    , EditComponent
    , ToolbarMultirowComponent
    // , OldComponent
  ],
  imports: [
    BrowserModule
    , HttpClientModule
    , ReactiveFormsModule
    , AppRoutingModule
    , BrowserAnimationsModule
    , MatToolbarModule
    , MatIconModule

    , MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule, MatButtonModule, MatCardModule, MatTableModule, MatDividerModule, MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
